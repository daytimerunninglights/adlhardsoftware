#include <main.h>
#include "PL.c"

// header function
void sendSettings();
void save_setting();
void load_setting();

//global
int1 mode_input1 = 0;
int1 mode_input2 = 0;
int1 mode_input3 = 0;
int1 mode_input4 = 0;
int1 mode_input5 = 0;
int1 mode_V_enable = 1;
int16 mode_V_level_hi = 576; //676
int16 mode_V_level_lo = 550; //650
int16 V_volume = 0;
int16 delay_level = 5000;

int1 mode_voltage_send = 0;
int1 enable_check_states = 0;
int1 output_state = 0;
int16 count_timer1 = 0;
int8 V_State = 1;
int16 delay_count = 0;
int16 delay_target = 0;
int16 threshold_count = 0;
int16 threshold_level = 137;

void OnReadData(byte channel) {
    mode_voltage_send = 1;
    switch (channel) {
        case 0:
            sendSettings();
            break;
        case 1:
            mode_input1 = PLGetInt8();
            break;
        case 2:
            mode_input2 = PLGetInt8();
            break;
        case 3:
            mode_input3 = PLGetInt8();
            break;
        case 4:
            mode_input4 = PLGetInt8();
            break;
        case 5:
            mode_input5 = PLGetInt8();
            break;
        case 6:
            mode_V_enable = PLGetInt8();
            break;
        case 7:
            mode_V_level_hi = PLGetInt16();
            break;
        case 8:
            mode_V_level_lo = PLGetInt16();
            break;
        case 11:
            save_setting();
            break;
        case 15:
            mode_voltage_send = 0;
            break;
        case 16:
            delay_level = PLGetInt16();
            break;    
    }

}

void sendSettings() {
    PLSendInt8(1, mode_input1);
    PLSendInt8(2, mode_input2);
    PLSendInt8(3, mode_input3);
    PLSendInt8(4, mode_input4);
    PLSendInt8(5, mode_input5);
    PLSendInt8(6, mode_V_enable);
    PLSendInt16(7, mode_V_level_hi);
    PLSendInt16(8, mode_V_level_lo);
    PLSendInt16(12, V_volume);
    PLSendInt16(16, delay_level);
    PLSendInt8(0, 2);
}

void sendInputs()
{
    int8 inp = 0;
    if (input_state(IN1)) bit_set(inp, 0);
    if (input_state(IN2)) bit_set(inp, 1);
    if (input_state(IN3)) bit_set(inp, 2);
    if (input_state(IN4)) bit_set(inp, 3);
    if (!input_state(IN5)) bit_set(inp, 4);
    PLSendInt8(14, inp);
}

void Threshold() {
    threshold_count++;
    if (threshold_count > threshold_level) {
        if (delay_count < delay_target) delay_count++;
        if (delay_count > delay_target) delay_count = delay_target;

        threshold_count = 0;
        if (delay_target == 0) output_state = 0;
        else if (delay_count >= delay_target) output_state = 1;
        if (output_state) output_high(PIN_C5);
        else output_low(PIN_C5);

    }
}

void checkState() {
    V_volume = read_adc();

    if (mode_V_enable) {
        if (V_volume > mode_V_level_hi) V_State = 0;
        if (V_volume < mode_V_level_lo) V_State = 1;
    } else V_State = 0;

    int1 outState = 0;
    outState = outState || (mode_input1 && input_state(IN1)) ||
            (mode_input2 && input_state(IN2)) ||
            (mode_input3 && !input_state(IN3)) ||
            (mode_input4 && input_state(IN4)) ||
            (mode_input5 && !input_state(IN5)) || V_State;

    if (!outState) delay_target = delay_level;
    else delay_target = 0;

    if (mode_voltage_send == 1) {
        count_timer1++;
        if (count_timer1 == 10) {
            PLSendInt16(12, V_volume);
            PLSendInt16(17, delay_count);
            PLSendInt8(18, output_state);
            sendInputs();
            count_timer1 = 0;
        }
    }
    enable_check_states = 0;
}

void load_setting() {
    if (read_eeprom(1) == 0xFF) return;
    mode_input1 = read_eeprom(1);
    mode_input2 = read_eeprom(2);
    mode_input3 = read_eeprom(3);
    mode_input4 = read_eeprom(4);
    mode_input5 = read_eeprom(5);
    mode_V_enable = read_eeprom(6);
    mode_V_level_hi = make16(read_eeprom(7), read_eeprom(8));
    mode_V_level_lo = make16(read_eeprom(9), read_eeprom(10));
    delay_level = make16(read_eeprom(13), read_eeprom(14));
}

void save_setting() {
    write_eeprom(1, mode_input1);
    write_eeprom(2, mode_input2);
    write_eeprom(3, mode_input3);
    write_eeprom(4, mode_input4);
    write_eeprom(5, mode_input5);
    write_eeprom(6, mode_V_enable);
    write_eeprom(7, (mode_V_level_hi >> 8));
    write_eeprom(8, (mode_V_level_hi & 0xFF));
    write_eeprom(9, (mode_V_level_lo >> 8));
    write_eeprom(10, (mode_V_level_lo & 0xFF));
    write_eeprom(13, (delay_level >> 8));
    write_eeprom(14, (delay_level & 0xFF));
    PLSendInt8(11, 1);
}

#INT_TIMER1

void TIMER1_isr(void) {
    enable_check_states = 1;
}

void main() {

    setup_wdt(WDT_ON);
    setup_wdt(WDT_2304MS | WDT_DIV_2); //~1,1 s reset

    port_B_pullups(0x50);
    setup_adc_ports(sAN2);
    setup_adc(ADC_CLOCK_DIV_2);

    setup_timer_1(T1_INTERNAL | T1_DIV_BY_2); //104 ms overflow
    output_low(PIN_C5);

    PLOnReadData = OnReadData;
    PLInit();

    set_adc_channel(2);
    read_adc(ADC_START_ONLY);
    set_pwm1_duty((int16) 0);

    load_setting();

    enable_interrupts(INT_TIMER1);
    enable_interrupts(GLOBAL);

    while (TRUE) {
        PLCheckMess();
        rs232_errors = 0;
        if (enable_check_states) checkState();
        Threshold();
        restart_wdt();
    }
}
